<?php
namespace app\api\controller;
use app\common\model\RougeAchi;
use app\common\model\RougeDistr;
use app\common\model\RougePaylog;
use app\common\model\RougeUser;
class Index
{
    public function index()
    {
    }
    //发放业绩
    /*
     *  openid 支付人  orderid 订单号  price 支付金额
     *
     */
    private function sendachi($param){
        $RougeUser = new RougeUser();
        $map['openid'] = $param['openid'];
        $user = $RougeUser->where($map)->find();
        //用户信息END
        $RougeDistr = new RougeDistr();
        $distr = $RougeDistr->where('uniacid', $user['uniacid'])->find();
        //分销信息END
        if ($distr) {
            //一级
            if (!empty($user['top_openid']) && !empty($distr['re_ratio'])) {
                $this->addachi($user['uniacid'],$user['openid'],$user['top_openid'],$distr['re_ratio'],$param['price'],$param['orderid']);
            }
            //一级END
            //二级
            if (!empty($user['two_openid']) && !empty($distr['two_re_ratio'])) {
                $this->addachi($user['uniacid'],$user['openid'],$user['two_openid'],$distr['two_re_ratio'],$param['price'],$param['orderid']);
            }
            //二级END
            //三级
            if (!empty($user['three_openid']) && !empty($distr['three_re_ratio'])) {
                $this->addachi($user['uniacid'],$user['openid'],$user['three_openid'],$distr['three_re_ratio'],$param['price'],$param['orderid']);
            }
            //三级END
        }
    }
    //增加业绩
    /*
     * uniacid   openid  top_openid  ratio  price  orderid
     */
    public function addachi($uniacid,$openid,$top_openid,$ratio,$price,$orderid){
        $RougeUser = new RougeUser();
        $RougeAchi = new RougeAchi();
        $bili = $ratio / 100;
        //END
        $data['uniacid'] = $uniacid;
        $data['top_openid'] = $top_openid;
        $data['orderid'] = $orderid;
        $data['openid'] = $openid;
        $data['price'] = $price;
        $data['re_ratio'] = $ratio;
        $data['get_price'] = $price * $bili;
        $data['create_time'] = time();
        $RougeAchi->allowField(true)->save($data);
        $RougeUser->where(['openid'=>$top_openid])->setInc('bonus',$data['get_price']);
    }
}
